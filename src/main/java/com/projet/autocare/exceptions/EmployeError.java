package com.projet.autocare.exceptions;

public class EmployeError extends Exception {

	public EmployeError() {
		// TODO Auto-generated constructor stub
	}

	public EmployeError(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmployeError(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public EmployeError(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmployeError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
