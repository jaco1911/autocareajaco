package com.projet.autocare.controllers;


import com.projet.autocare.entities.Employe;
import com.projet.autocare.exceptions.EmployeError;
import com.projet.autocare.repository.EmployeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/employe")
public class EmployeController {
    @Autowired
    EmployeRepository employeRepository;

    @GetMapping
    public String welcome() {
        return "Welcome REST CONTROLLER : EMPLOYE";
    }


    @GetMapping("/all")
    public String findAll(Model model) {
        model.addAttribute("employes", employeRepository.findAll());
        model.addAttribute("titre", "Liste des employes");
        return "employe/employeList";
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("employeForm", new Employe());
        model.addAttribute("titre", "Formulaire d'ajout d'un employe");
        return "employe/add";
    }

    @PostMapping("/add")
    public String addEmploye(@Valid @ModelAttribute("employeForm") Employe employeForm) {
        employeRepository.save(employeForm);
        return "redirect:/employe/all";
    }

    @GetMapping("/update/{id}")
    public String updateEmploye(Model model, @PathVariable("id") Integer id) {
        model.addAttribute("employeForm", new Employe());
        model.addAttribute("titre", "Modification de la fiche employe");
        model.addAttribute("employe", employeRepository.findById(id).get());
        return "employe/updateEmploye";
    }


    @PostMapping("/update/{id}")
    public String updateEmploye(@PathVariable("id") Integer id,
                               @Valid @ModelAttribute("employeForm") Employe employeForm) throws EmployeError {
        Optional<Employe> c = employeRepository.findById(id);
        if (c.isEmpty()) {
            throw (new EmployeError("Employe id :" + id + " non trouvé : impossible pour faire un update"));
        }
        employeRepository.save(employeForm);
        return "redirect:/employe/all";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id) throws Exception {
        Optional<Employe> employe = employeRepository.findById(id);
        if (employe.isEmpty()) {
            throw (new Exception("Employe id :" + id + " non trouvé !"));
        }

        employeRepository.deleteById(id);
        return "redirect:/employe/all";
    }

    @ExceptionHandler(value = {EmployeError.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String errorEmployeException(EmployeError e) {

        return "Soucis sur le controlleur : " + this.getClass().getSimpleName() + ":" + e.getMessage();
    }
}
