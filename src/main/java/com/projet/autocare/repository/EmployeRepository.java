package com.projet.autocare.repository;


import com.projet.autocare.entities.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface EmployeRepository extends JpaRepository<Employe, Integer> {

    @Query(" select employe from Employe employe " +
            " where employe.nom_utilisateur_employe = ?1")
    Optional<Employe> findUserWithName(String username);

}
