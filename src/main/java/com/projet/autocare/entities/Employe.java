package com.projet.autocare.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "EMPLOYE")
public class Employe implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_employe;

    private String nom_utilisateur_employe;

    private String mot_de_passe_employe;

    private String nom_employe;

    private String prenom_employe;

    public String getNom_employe() {
        return nom_employe;
    }

    public void setNom_employe(String nom_employe) {
        this.nom_employe = nom_employe;
    }

    public String getPrenom_employe() {
        return prenom_employe;
    }

    public void setPrenom_employe(String prenom_employe) {
        this.prenom_employe = prenom_employe;
    }

    /**
     * @return
     * @OnetoMany List <Profil> lProfil;
     */

    public Integer getId_employe() {
        return id_employe;
    }

    public void setId_employe(Integer id_employe) {
        this.id_employe = id_employe;
    }

    public String getNom_utilisateur_employe() {
        return nom_utilisateur_employe;
    }

    public void setNom_utilisateur_employe(String nom_utilisateur_employe) {
        this.nom_utilisateur_employe = nom_utilisateur_employe;
    }

    public String getMot_de_passe_employe() {
        return mot_de_passe_employe;
    }

    public void setMot_de_passe_employe(String mot_de_passe_employe) {
        this.mot_de_passe_employe = mot_de_passe_employe;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //Faire des liens les profils
        return null;
    }

    public String getPassword() {
        return mot_de_passe_employe;
    }

    public void setPassword(String mot_de_passe_employe) {
        this.mot_de_passe_employe = mot_de_passe_employe;
    }

    public String getUsername() {
        return nom_utilisateur_employe;
    }

    public void setUsername(String nom_utilisateur_employe) {
        this.nom_utilisateur_employe = nom_utilisateur_employe;
    }
}
